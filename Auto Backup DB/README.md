# Auto Backup of MongoDB Database

## Prerequisites

1. Install schedule with pip
```bash
pip install schedule
```

2. Install the Mongo Database tools (get the latest url via https://www.mongodb.com/try/download/database-tools):
```bash
wget https://fastdl.mongodb.org/tools/db/mongodb-database-tools-ubuntu2204-x86_64-100.8.0.tgz
tar -xvzf mongodb-database-tools-ubuntu2204-x86_64-100.8.0.tgz
sudo cp -R mongodb-database-tools-ubuntu2204-x86_64-100.8.0/bin/* /usr/local/bin/
```

Check that the installation was successful :
```bash
mongodump --version
```


## How it works

1. Create a credentials file (with restricted access) called credentials.ini using the template file `credentials.tmpl`

N.B: Don't forget to protect the credentials file e.g.
```bash
sudo chown root:root credentials.ini
chmod 400 credentials.ini
```

2. Execute the script in background with log redirected in a log file:
```bash
nohup python3 script_auto_save_mongodb.py > log_script_auto_backup_db.log 2>&1 &
```

`N.B.: You can update the interval time for auto backup before running the script.`

3. Check the log file `log_script_auto_backup_db.log`

4. Check the backup file in the backup path you choose.

5. To stop the script, 
* get the PID of the process running the script
```bash
ps -aux | grep python3
```
* kill the process (replace xxx with the PID of the process)
```bash
kill xxxx
```

