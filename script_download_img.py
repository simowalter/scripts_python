import json
import os
import requests
from urllib.parse import urlparse

# Define the function to download images
def download_images(urls, folder_name):
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)

    for index, url in enumerate(urls, 1):
        try:
            response = requests.get(url)
            if response.status_code == 200:
                # Extract the filename from the URL
                filename = os.path.join(folder_name, f"{folder_name}_{index:03d}{os.path.splitext(urlparse(url).path)[1]}")

                # Save the image
                with open(filename, 'wb') as file:
                    file.write(response.content)
                print(f"Downloaded: {filename}")
            else:
                print(f"Failed to download: {url}")
        except Exception as e:
            print(f"Error while downloading {url}: {str(e)}")

# Read the text file
with open("image_urls.json", "r") as file:
    data = file.read()

# Parse the text data as JSON
data = json.loads(data)

# Process each keyword and its associated URLs
for keyword, urls in data.items():
    download_images(urls, keyword)
