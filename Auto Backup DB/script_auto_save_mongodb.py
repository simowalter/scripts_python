import subprocess
import schedule
import time
import configparser

# Load credentials from the "credentials.ini" file
config = configparser.ConfigParser()
config.read('credentials.ini')

# MongoDB configuration
mongo_host = config.get('database', 'host')
mongo_port = int(config.get('database', 'port'))
mongo_username = config.get('database', 'username')
mongo_password = config.get('database', 'password')
backup_dir = config.get('database', 'backup_dir')


def backup_mongodb():
    # Get the current time for the backup file timestamp
    current_time = time.strftime("%Y-%m-%d-%H-%M-%S")

    # Name of the backup file
    backup_file = f"backup_{current_time}.bson"

    
    mongo_uri = "mongodb://"+mongo_username+":"+mongo_password+"@"+mongo_host+":"+str(mongo_port)
    backup_command = f"mongodump {mongo_uri} --archive={backup_dir}{backup_file}"


    try:
        # Execute the backup command
        subprocess.run(backup_command, shell=True, check=True)
        print(f"Backup successful in {backup_dir}{backup_file}")
    except subprocess.CalledProcessError as e:
        print(f"Error during backup: {e}")

# create backup path if it does not exist
subprocess.run(f"mkdir -p {backup_dir}", shell=True, check=True)

# Schedule backups every 24 hours
schedule.every(24).hours.do(backup_mongodb)

while True:
    schedule.run_pending()
    time.sleep(1)