import os
from PIL import Image

def convert_pngs_in_folder(input_folder, output_folder, quality=95):
    try:
        os.makedirs(output_folder, exist_ok=True)
        for root, _, files in os.walk(input_folder):
            for file in files:
                if file.lower().endswith(".png"):
                    png_image_path = os.path.join(root, file)
                    jpeg_image_name = os.path.splitext(file)[0] + ".jpg"
                    jpeg_image_path = os.path.join(output_folder, jpeg_image_name)

                    image = Image.open(png_image_path)
                    image = image.convert("RGB")
                    image.save(jpeg_image_path, "JPEG", quality=quality)
                    print(f"Converted: {png_image_path} -> {jpeg_image_path}")

        print("Conversion complete.")
    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    input_folder = "png_folder"  # Replace with the path to your input folder
    output_folder = "jpeg_output"  # Replace with the path to your output folder
    quality = 100  # You can adjust the quality (0-100)

    convert_pngs_in_folder(input_folder, output_folder, quality)
