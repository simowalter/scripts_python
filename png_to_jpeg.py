from PIL import Image

def convert_png_to_jpeg(png_image_path, jpeg_image_path, quality=95):
    try:
        image = Image.open(png_image_path)
        image = image.convert("RGB")
        image.save(jpeg_image_path, "JPEG", quality=quality)
        print(f"Conversion successful. Image saved as {jpeg_image_path}")
    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    png_image_path = "input_image.png"  # Replace with the path to your PNG image
    jpeg_image_path = "output_image.jpg"  # Replace with the desired output JPEG path
    quality = 100  # You can adjust the quality (0-100)

    convert_png_to_jpeg(png_image_path, jpeg_image_path, quality)
